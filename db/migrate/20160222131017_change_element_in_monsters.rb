class ChangeElementInMonsters < ActiveRecord::Migration
  def up
    change_column_default :monsters, :element, nil
  end

  def down
    change_column_default :monsters, :element, 'fire'
  end
end
