class AddTitleToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :title, :string
    add_index :teams, [:user_id, :title], unique: true
  end
end
