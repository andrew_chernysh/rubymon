class RemovePositionFromMonsters < ActiveRecord::Migration
  def change
    remove_column :monsters, :position, :integer, null: false, default: 0
  end
end
