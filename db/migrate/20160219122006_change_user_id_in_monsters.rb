class ChangeUserIdInMonsters < ActiveRecord::Migration
  def change
    change_column_null :monsters, :user_id, false
  end
end
