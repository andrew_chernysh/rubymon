class CreateMonsters < ActiveRecord::Migration
  def change
    create_table :monsters do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :power
      t.string :element, null: false, default: 'fire'
      t.integer :position, null: false, default: 0

      t.timestamps null: false

      t.index :name, unique: true
    end
  end
end
