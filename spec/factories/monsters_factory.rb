FactoryGirl.define do
  factory :monster do
    user
    team nil
    sequence(:name) { |n| "#{Faker::Team.creature.singularize} #{n}" }
    element { Monster::TYPES.sample }
    power { (1..100).to_a.sample }
  end

  trait :with_team do
    team
  end

  factory :monster_of_fire, parent: :monster do
    element 'fire'
  end

  factory :monster_of_water, parent: :monster do
    element 'water'
  end

  factory :monster_of_earth, parent: :monster do
    element 'earth'
  end

  factory :monster_of_electric, parent: :monster do
    element 'electric'
  end

  factory :monster_of_wind, parent: :monster do
    element 'wind'
  end
end
