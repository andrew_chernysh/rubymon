FactoryGirl.define do
  factory :team do
    user
    sequence(:title) { |n| "#{Faker::Lorem.word} #{n}" }
  end

  trait :with_monsters do
    after(:build) do |team|
      team.monsters = FactoryGirl.build_list(:monster, 3, user: team.user)
    end
  end
end
