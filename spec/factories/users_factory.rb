FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    provider 'facebook'
    uid { SecureRandom.hex(6) }
    token { SecureRandom.hex(10) }
    expires_at { 1.month.from_now }
  end
end
