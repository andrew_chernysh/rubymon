require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe 'GET #new' do
    it 'should render sign in page successfully' do
      get :new

      expect(response).to be_successful
      expect(response).to render_template :new
      expect(assigns(:page_title)).to eq 'Sign In'
    end
  end

  describe 'GET #auth_callback' do
    context 'when success' do
      let(:user) { FactoryGirl.create(:user) }

      it 'should sign in user and redirect to root page with success message' do
        expect(User).to receive(:from_omniauth!).once.and_return(user)

        get :auth_callback

        expect(response).to be_redirect
        expect(response).to redirect_to root_path
        expect(session[:user_id]).to eq user.id
        expect(flash[:success]).to eq 'Successfully authenticated user'
      end
    end

    context 'when failed' do
      it 'should redirect to sign in page with danger message' do
        expect(User).to receive(:from_omniauth!).once.and_raise(StandardError, 'Some error')

        get :auth_callback

        expect(response).to be_redirect
        expect(response).to redirect_to sign_in_path
        expect(session[:user_id]).to be_nil
        expect(flash[:danger]).to eq 'Unable to authenticate user. Error happened: Some error'
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:user) { FactoryGirl.create(:user) }

    before { session[:user_id] = user.id }

    it 'should clear user session and redirect to sign in page' do
      delete :destroy

      expect(response).to be_redirect
      expect(response).to redirect_to sign_in_path
      expect(session[:user_id]).to be_nil
      expect(flash[:success]).to eq 'Successfully signed out user'
    end
  end
end
