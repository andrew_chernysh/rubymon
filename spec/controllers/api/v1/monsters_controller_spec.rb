require 'rails_helper'

RSpec.describe Api::V1::MonstersController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }

  before { api_authorization_header(user.token) }

  describe 'GET #index' do
    let!(:monster1) { FactoryGirl.create(:monster, user: user) }
    let!(:monster2) { FactoryGirl.create(:monster, user: user) }
    let!(:monster3) { FactoryGirl.create(:monster, user: user) }
    let!(:monster4) { FactoryGirl.create(:monster) }

    it 'should respond with appropriate monsters data' do
      get :index

      expect(response.status).to eq 200
      expect(json_response[:monsters].map { |monster| monster[:id] }).to eq [monster1.id, monster2.id, monster3.id]
      expect(json_response[:monsters].first.keys).to eq [:id, :team_id, :name, :power, :element, :strength, :weakness]
    end
  end

  describe 'GET #show' do
    let(:monster) { FactoryGirl.create(:monster, user: user) }

    it 'should respond with appropriate monster data' do
      get :show, id: monster.id

      expect(response.status).to eq 200
      expect(json_response[:monster][:id]).to eq monster.id
      expect(json_response[:monster].keys).to eq [:id, :team_id, :name, :power, :element, :strength, :weakness]
    end
  end

  describe 'POST #create' do
    context 'when success' do
      it 'should create new monster and respond with its data' do
        expect { post :create, monster: FactoryGirl.attributes_for(:monster, user_id: user.id) }.to change { Monster.count }.by(1)

        expect(response.status).to eq 201
        expect(json_response.key?(:errors)).to be_falsey
        expect(json_response[:monster].keys).to eq [:id, :team_id, :name, :power, :element, :strength, :weakness]
      end
    end

    context 'when failed' do
      it 'should not create new monster and respond with errors data' do
        expect { post :create, monster: FactoryGirl.attributes_for(:monster, name: nil, user_id: user.id) }.not_to change { Monster.count }

        expect(response.status).to eq 422
        expect(json_response.key?(:errors)).to be_truthy
        expect(json_response[:errors][:name]).to include "can't be blank"
      end
    end
  end

  describe 'PATCH #update' do
    let(:monster) { FactoryGirl.create(:monster, user: user) }

    context 'when success' do
      it 'should update monster and respond with its data' do
        patch :update, id: monster.id, monster: { name: 'new_name_for_monster' }

        expect(response.status).to eq 200
        expect(json_response.key?(:errors)).to be_falsey
        expect(json_response[:monster].keys).to eq [:id, :team_id, :name, :power, :element, :strength, :weakness]
        expect(json_response[:monster][:name]).to eq 'new_name_for_monster'
      end
    end

    context 'when failed' do
      it 'should not update monster and respond with errors data' do
        patch :update, id: monster.id, monster: { name: nil }

        expect(response.status).to eq 422
        expect(json_response.key?(:errors)).to be_truthy
        expect(json_response[:errors][:name]).to include "can't be blank"
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:monster) { FactoryGirl.create(:monster, user: user) }

    context 'when success' do
      it 'should destroy monster record and respond with no content' do
        delete :destroy, id: monster.id

        expect(response.status).to eq 204
        expect(assigns(:monster)).to be_destroyed
        expect(response.body).to be_blank
      end
    end

    context 'when failed' do
      it 'should destroy monster record and respond with no content' do
        delete :destroy, id: 999

        expect(response.status).to eq 400
        expect(json_response[:errors]).to eq 'Bad request'
      end
    end
  end
end
