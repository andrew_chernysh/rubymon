require 'rails_helper'

RSpec.describe Api::V1::TeamsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }

  before { api_authorization_header(user.token) }

  describe 'GET #index' do
    let!(:team1) { FactoryGirl.create(:team, :with_monsters, user: user) }
    let!(:team2) { FactoryGirl.create(:team, user: user) }
    let!(:team3) { FactoryGirl.create(:team, user: user) }
    let!(:team4) { FactoryGirl.create(:team) }

    it 'should respond with user teams data' do
      get :index

      expect(response.status).to eq 200
      expect(json_response.key?(:teams)).to be_truthy
      expect(json_response[:teams].count).to eq 3
      expect(json_response[:teams].first.keys).to eq [:id, :user_id, :title, :created_at, :updated_at, :monster_ids]
      expect(json_response[:teams].map { |team| team[:id] }.sort).to eq [team1.id, team2.id, team3.id]
    end
  end

  describe 'GET #show' do
    let(:team) { FactoryGirl.create(:team, :with_monsters, user: user) }

    it 'should respond with team data' do
      get :show, id: team.id

      expect(response.status).to eq 200
      expect(json_response.key?(:team)).to be_truthy
      expect(json_response[:team].keys).to eq [:id, :user_id, :title, :created_at, :updated_at, :monster_ids]
      expect(json_response[:team][:id]).to eq team.id
    end
  end

  describe 'POST #create' do
    let!(:monster) { FactoryGirl.create(:monster, user: user, team: nil) }

    context 'when success' do
      before { post :create, team: { title: 'AAA', monster_ids: [monster.id] } }

      it 'should create team with monsters for user' do
        expect(user.reload.teams.count).to eq 1
        expect(assigns(:team).monsters.count).to eq 1
      end

      it 'should respond with appropriate team data' do
        expect(response.status).to eq 201
        expect(json_response.key?(:team)).to be_truthy
        expect(json_response[:team].keys).to eq [:id, :user_id, :title, :created_at, :updated_at, :monster_ids]
        expect(json_response[:team][:monster_ids]).to eq [monster.id]
      end
    end

    context 'when failed' do
      before { post :create, team: { monster_ids: [monster.id] } }

      it 'should not create team with monsters for user' do
        expect(user.teams.count).to eq 0
      end

      it 'should respond with errors data' do
        expect(response.status).to eq 422
        expect(json_response.key?(:team)).to be_falsey
        expect(json_response.key?(:errors)).to be_truthy
      end
    end
  end

  describe 'PATCH #update' do
    let(:team) { FactoryGirl.create(:team, user: user, monsters: [FactoryGirl.build(:monster, user: user, team: nil)]) }

    context 'when success' do
      it 'should update team and respond with team data' do
        patch :update, id: team.id, team: { title: 'AAA' }

        expect(response.status).to eq 200
        expect(json_response.key?(:team)).to be_truthy
        expect(json_response[:team].keys).to eq [:id, :user_id, :title, :created_at, :updated_at, :monster_ids]
        expect(json_response[:team][:monster_ids].count).to eq 1
      end
    end

    context 'when failed' do
      it 'should update team and respond with team data' do
        patch :update, id: team.id, team: { title: '' }

        expect(response.status).to eq 422
        expect(json_response.key?(:team)).to be_falsey
        expect(json_response.key?(:errors)).to be_truthy
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:team) { FactoryGirl.create(:team, user: user) }

    context 'when success' do
      it 'should destroy team record and respond with no content' do
        delete :destroy, id: team.id

        expect(response.status).to eq 204
        expect(assigns(:team)).to be_destroyed
        expect(response.body).to be_blank
      end
    end

    context 'when failed' do
      it 'should destroy team record and respond with no content' do
        delete :destroy, id: 999

        expect(response.status).to eq 400
        expect(json_response[:errors]).to eq 'Bad request'
      end
    end
  end
end
