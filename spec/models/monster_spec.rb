require 'rails_helper'

RSpec.describe Monster, type: :model do
  let(:monster_of_fire) { FactoryGirl.build(:monster_of_fire) }
  let(:monster_of_fire2) { FactoryGirl.build(:monster_of_fire) }
  let(:monster_of_water) { FactoryGirl.build(:monster_of_water) }
  let(:monster_of_earth) { FactoryGirl.build(:monster_of_earth) }
  let(:monster_of_electric) { FactoryGirl.build(:monster_of_electric) }
  let(:monster_of_wind) { FactoryGirl.build(:monster_of_wind) }

  describe 'associations' do
    it 'should belong to user and team' do
      expect(subject).to belong_to :user
      expect(subject).to belong_to :team
    end
  end

  describe 'validations' do
    it 'should validate presence of user, name and element' do
      expect(subject).to validate_presence_of :user
      expect(subject).to validate_presence_of :name
      expect(subject).to validate_presence_of :element
    end

    it 'should validate associated user' do
      user = FactoryGirl.build(:user, email: nil)
      monster = FactoryGirl.build(:monster, user: user)

      expect(monster.valid?).to be_falsey
      expect(monster.errors.full_messages).to eq ['User is invalid']
    end

    it 'should validate associated team' do
      user = FactoryGirl.create(:user)

      team1 = FactoryGirl.create(:team, user: user)
      monster1 = FactoryGirl.build(:monster, user: user, team: nil)

      team2 = FactoryGirl.build(:team, user: user)
      monster2 = FactoryGirl.build(:monster, user: user, team: nil)

      team1.title = nil
      team1.save(validate: false)

      team1.monsters << monster1
      team2.monsters << monster2

      expect(monster1.valid?).to be_falsey
      expect(monster1.errors.full_messages).to include 'Team is invalid'
      expect(monster2.valid?).to be_truthy
    end

    it 'should validate if team has free place for monster' do
      user = FactoryGirl.create(:user)

      team1 = FactoryGirl.create(:team, user: user, monsters: FactoryGirl.build_list(:monster, 3, user: user, team: nil))
      monster1 = FactoryGirl.build(:monster, user: user, team: team1)

      team2 = FactoryGirl.create(:team, user: user, monsters: FactoryGirl.build_list(:monster, 2, user: user, team: nil))
      monster2 = FactoryGirl.build(:monster, user: user, team: team2)

      expect(monster1.valid?).to be_falsey
      expect(monster1.errors.full_messages).to include 'Team is already full'
      expect(monster2.valid?).to be_truthy
    end

    it 'should validate uniqueness of name' do
      monster = FactoryGirl.build(:monster, user: FactoryGirl.create(:user))
      expect(monster).to validate_uniqueness_of(:name)
    end

    it 'should validate inclusion of element in monster types' do
      expect(subject).to validate_inclusion_of(:element).in_array(Monster::TYPES)
    end
  end

  describe 'scopes' do
    describe '.without_team' do
      let!(:monster1) { FactoryGirl.create(:monster) }
      let!(:monster2) { FactoryGirl.create(:monster, :with_team) }
      let!(:monster3) { FactoryGirl.create(:monster) }

      it 'should return monsters without teams' do
        expect(Monster.without_team.sort_by(&:id)).to eq [monster1, monster3]
      end
    end
  end

  describe '#strength' do
    it 'should return correct strength for every type of monster' do
      expect(monster_of_fire.strength).to eq 'wind'
      expect(monster_of_water.strength).to eq 'fire'
      expect(monster_of_earth.strength).to eq 'water'
      expect(monster_of_electric.strength).to eq 'earth'
      expect(monster_of_wind.strength).to eq 'electric'
    end
  end

  describe '#weakness' do
    it 'should return correct weakness for every type of monster' do
      expect(monster_of_fire.weakness).to eq 'water'
      expect(monster_of_water.weakness).to eq 'earth'
      expect(monster_of_earth.weakness).to eq 'electric'
      expect(monster_of_electric.weakness).to eq 'wind'
      expect(monster_of_wind.weakness).to eq 'fire'
    end
  end
end
