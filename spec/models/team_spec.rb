require 'rails_helper'

RSpec.describe Team, type: :model do
  describe 'associations' do
    it 'should belong_to user' do
      expect(subject).to belong_to :user
    end

    it 'should have many monsters' do
      expect(subject).to have_many(:monsters).dependent(:nullify)
    end
  end

  describe 'validations' do
    let(:user) { FactoryGirl.create(:user) }

    it 'should validate presence of title and user' do
      expect(subject).to validate_presence_of :title
      expect(subject).to validate_presence_of :user
    end

    it 'should validate associated user' do
      user = FactoryGirl.build(:user, email: nil)
      team = FactoryGirl.build(:team, user: user)

      expect(team.valid?).to be_falsey
      expect(team.errors.full_messages).to eq ['User is invalid']
    end

    it 'should validate uniqueness of title scoped to user' do
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)

      FactoryGirl.create(:team, user: user1, title: 'AAA')
      team1 = FactoryGirl.build(:team, user: user1, title: 'AAA')
      team2 = FactoryGirl.build(:team, user: user2, title: 'AAA')

      expect(team1.valid?).to be_falsey
      expect(team1.errors.full_messages).to eq ['Title has already been taken']
      expect(team2.valid?).to be_truthy
    end

    it 'should validate amount of monsters in a team' do
      valid_team = FactoryGirl.build(:team, user: user, monsters: FactoryGirl.build_list(:monster, 3, user: user))
      invalid_team = FactoryGirl.build(:team, user: user, monsters: FactoryGirl.build_list(:monster, 4, user: user))

      expect(valid_team.valid?).to be_truthy
      expect(invalid_team.valid?).to be_falsey
      expect(invalid_team.errors.full_messages).to include 'Monsters can be maximum 3 in a team'
    end

    it 'should validate if monsters do not belong to other teams' do
      monster1 = FactoryGirl.build(:monster, user: user)
      monster2 = FactoryGirl.build(:monster, user: user)
      monster3 = FactoryGirl.build(:monster, :with_team, user: user)

      FactoryGirl.create(:team, user: user, monsters: [monster1, monster2])
      invalid_team = FactoryGirl.build(:team, user: user, monsters: [monster2, monster3])

      expect(invalid_team.valid?).to be_falsey
      expect(invalid_team.errors.full_messages).to include 'Monsters must contain monsters which do not belong to other teams'
    end

    it 'should validate if monsters belong to a team owner' do
      monster1 = FactoryGirl.build(:monster, user: user, team: nil)
      monster2 = FactoryGirl.build(:monster, team: nil)

      team1 = FactoryGirl.build(:team, user: user, monsters: [monster1])
      team2 = FactoryGirl.build(:team, user: user, monsters: [monster2])

      expect(team1.valid?).to be_truthy
      expect(team2.valid?).to be_falsey
      expect(team2.errors.full_messages).to include 'Monsters must belong to an owner of the team'
    end
  end
end
