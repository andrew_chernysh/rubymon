require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it 'should have many monsters and teams' do
      expect(subject).to have_many(:monsters).dependent(:delete_all)
      expect(subject).to have_many(:teams).dependent(:delete_all)
    end
  end

  describe 'validations' do
    it 'should validate presence of email, name, provider, uid and token' do
      expect(subject).to validate_presence_of :email
      expect(subject).to validate_presence_of :name
      expect(subject).to validate_presence_of :provider
      expect(subject).to validate_presence_of :uid
      expect(subject).to validate_presence_of :token
    end

    it 'should validate uniqueness of email, uid and token' do
      expect(subject).to validate_uniqueness_of :email
      expect(subject).to validate_uniqueness_of :uid
      expect(subject).to validate_uniqueness_of :token
    end

    it 'should validate amount of monsters' do
      valid_user = FactoryGirl.build(:user, monsters: FactoryGirl.build_list(:monster, 20))
      invalid_user = FactoryGirl.build(:user, monsters: FactoryGirl.build_list(:monster, 21))

      expect(valid_user.valid?).to be_truthy
      expect(invalid_user.valid?).to be_falsey
      expect(invalid_user.errors.full_messages).to include 'Monsters can be maximum 20 for a user'
    end

    it 'should validate amount of teams' do
      valid_user = FactoryGirl.build(:user, teams: FactoryGirl.build_list(:team, 3))
      invalid_user = FactoryGirl.build(:user, teams: FactoryGirl.build_list(:team, 4))

      expect(valid_user.valid?).to be_truthy
      expect(invalid_user.valid?).to be_falsey
      expect(invalid_user.errors.full_messages).to include 'Teams can be maximum 3 for a user'
    end
  end

  describe '.from_omniauth!' do
    subject { User.from_omniauth!(auth) }

    context 'when user record exists in database' do
      let!(:user) { FactoryGirl.create(:user) }
      let(:auth) do
        double 'auth', provider: 'facebook',
                       uid: user.uid.to_s,
                       info: double('info', email: user.email, name: user.name),
                       credentials: double('credentials', token: user.token, expires_at: user.expires_at)
      end

      it 'should not create new user' do
        expect { User.from_omniauth!(auth) }.not_to change { User.count }
      end

      it 'should return appropriate user' do
        expect(User.from_omniauth!(auth)).to eq user
      end
    end

    context 'when user record does not exist in database' do
      let(:auth) do
        double 'auth', provider: 'facebook',
                       uid: '777777',
                       info: double('info', email: 'john.doe@example.com', name: 'John Doe'),
                       credentials: double('credentials', token: 'sometoken', expires_at: 1_460_983_535)
      end

      it 'should create new user' do
        expect { User.from_omniauth!(auth) }.to change { User.count }.from(0).to(1)
      end

      it 'should return recently created user' do
        user = User.from_omniauth!(auth)

        expect(user).to be_persisted
        expect(user.email).to eq 'john.doe@example.com'
        expect(user.name).to eq 'John Doe'
        expect(user.uid).to eq '777777'
        expect(user.provider).to eq 'facebook'
        expect(user.token).to eq 'sometoken'
        expect(user.expires_at).to eq Time.zone.at(1_460_983_535)
      end
    end

    context 'when invalid auth data' do
      let(:auth) do
        double 'auth', provider: 'facebook',
                       uid: '777777',
                       info: double('info', email: nil, name: 'John Doe'),
                       credentials: double('credentials', token: 'sometoken', expires_at: 1_460_983_535)
      end

      it 'should raise exception' do
        expect { User.from_omniauth!(auth) }.to raise_exception(ActiveRecord::RecordInvalid)
      end
    end
  end

  describe '#max_monsters?' do
    let(:user1) { FactoryGirl.create(:user, monsters: FactoryGirl.build_list(:monster, 2)) }
    let(:user2) { FactoryGirl.create(:user, monsters: FactoryGirl.build_list(:monster, 1)) }

    before { stub_const('User::MAX_MONSTERS_AMOUNT', 2) }

    it 'should return true when user reached the max number of monsters' do
      expect(user1.max_monsters?).to be_truthy
      expect(user2.max_monsters?).to be_falsey
    end
  end

  describe '#max_teams?' do
    let(:user1) { FactoryGirl.create(:user, teams: FactoryGirl.build_list(:team, 2)) }
    let(:user2) { FactoryGirl.create(:user, teams: FactoryGirl.build_list(:team, 1)) }

    before { stub_const('User::MAX_TEAMS_AMOUNT', 2) }

    it 'should return true when user reached the max number of teams' do
      expect(user1.max_teams?).to be_truthy
      expect(user2.max_teams?).to be_falsey
    end
  end
end
