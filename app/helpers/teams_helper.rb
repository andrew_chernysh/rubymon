module TeamsHelper
  def monsters_list(team)
    team.monsters.map { |monster| link_to(monster.name, edit_monster_path(monster)) }.join(', ').html_safe
  end
end
