class MonsterSerializer < ActiveModel::Serializer
  attributes :id, :team_id, :name, :power, :element, :strength, :weakness
end
