class TeamSerializer < ActiveModel::Serializer
  embed :ids

  attributes :id, :user_id, :title, :created_at, :updated_at

  has_many :monsters
end
