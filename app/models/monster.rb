class Monster < ActiveRecord::Base
  include Comparable

  TYPES = %w(fire water earth electric wind).freeze

  belongs_to :user
  belongs_to :team

  scope :without_team, -> { where(team: nil) }

  validates_associated :user, :team
  validates_presence_of :user, :name, :element
  validates_uniqueness_of :name
  validates_inclusion_of :element, in: TYPES
  validate :validate_team_size

  def validate_team_size
    errors.add(:team, 'is already full') if team.present? && team.monsters.count >= Team::MAX_MONSTERS_AMOUNT
  end

  def strength
    TYPES[element_index - 1]
  end

  def weakness
    TYPES[element_index + 1] || TYPES.first
  end

  private

  def element_index
    TYPES.index(element)
  end
end
