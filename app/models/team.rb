class Team < ActiveRecord::Base
  MAX_MONSTERS_AMOUNT = 3

  belongs_to :user
  has_many :monsters, dependent: :nullify

  validates_presence_of :title, :user
  validates_uniqueness_of :title, scope: [:user_id]
  validates_associated :user
  validate :validate_number_of_monsters, :validate_monsters_team, :validate_monsters_owner

  private

  def validate_number_of_monsters
    errors.add(:monsters, "can be maximum #{MAX_MONSTERS_AMOUNT} in a team") if monsters.size > MAX_MONSTERS_AMOUNT
  end

  def validate_monsters_team
    unless monsters.all? { |monster| monster.team_id.present? && monster.team_id == id || monster.team_id.nil? }
      errors.add(:monsters, 'must contain monsters which do not belong to other teams')
    end
  end

  def validate_monsters_owner
    unless monsters.all? { |monster| monster.user_id.present? && monster.user_id == user_id }
      errors.add(:monsters, 'must belong to an owner of the team')
    end
  end
end
