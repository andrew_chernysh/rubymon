class User < ActiveRecord::Base
  MAX_MONSTERS_AMOUNT = 20
  MAX_TEAMS_AMOUNT = 3

  has_many :monsters, dependent: :delete_all
  has_many :teams, dependent: :delete_all

  validates :email, :name, :provider, :uid, :token, presence: true
  validates :email, :uid, :token, uniqueness: true
  validate :validate_number_of_monsters, :validate_number_of_teams

  def self.from_omniauth!(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create!(
      email:      auth.info.email,
      name:       auth.info.name,
      provider:   auth.provider,
      uid:        auth.uid,
      token:      auth.credentials.token,
      expires_at: Time.zone.at(auth.credentials.expires_at)
    )
  end

  def max_monsters?
    monsters.count >= MAX_MONSTERS_AMOUNT
  end

  def max_teams?
    teams.count >= MAX_TEAMS_AMOUNT
  end

  private

  def validate_number_of_monsters
    errors.add(:monsters, "can be maximum #{MAX_MONSTERS_AMOUNT} for a user") if monsters.size > MAX_MONSTERS_AMOUNT
  end

  def validate_number_of_teams
    errors.add(:teams, "can be maximum #{MAX_TEAMS_AMOUNT} for a user") if teams.size > MAX_TEAMS_AMOUNT
  end
end
