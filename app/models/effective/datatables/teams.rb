module Effective
  module Datatables
    class Teams < Effective::Datatable
      datatable do
        default_entries :all
        default_order :title

        table_column :title
        array_column :monsters, filter: false, sortable: false do |team|
          team.monsters.map { |monster| link_to(monster.name, edit_monster_path(monster)).html_safe }.join(', ')
        end

        actions_column
      end

      def collection
        Team.includes(:monsters).where(user_id: attributes[:user_id])
      end
    end
  end
end
