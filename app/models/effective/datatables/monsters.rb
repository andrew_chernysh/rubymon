module Effective
  module Datatables
    class Monsters < Effective::Datatable
      datatable do
        default_entries 10
        default_order :element

        table_column :created_at, visible: false
        table_column :name
        table_column :power
        table_column :element, filter: { type: :select, values: Monster::TYPES }
        array_column :strength, filter: { type: :select, values: Monster::TYPES }
        array_column :weakness, filter: { type: :select, values: Monster::TYPES }
        table_column :team, column: 'teams.title', filter: { values: proc { Team.pluck(:title) } } do |monster|
          link_to(monster.team_title, team_path(monster.team_id)) if monster.team_id.present?
        end

        actions_column(show: false)
      end

      def collection
        relation = Monster.joins('LEFT JOIN teams ON monsters.team_id = teams.id')
                          .where(user_id: attributes[:user_id])
                          .select('monsters.*', 'teams.title AS team_title')

        attributes[:team_id].present? ? relation.where(team_id: attributes[:team_id]) : relation
      end
    end
  end
end
