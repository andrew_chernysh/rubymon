class MonstersController < BaseController
  before_filter :find_monster, only: [:edit, :update, :destroy]

  def index
    @monsters = Effective::Datatables::Monsters.new(user_id: current_user.id)
    @page_title = 'Monsters'
  end

  def new
    @monster = current_user.monsters.build
    @page_title = 'New Monster'
  end

  def create
    @monster = current_user.monsters.build(monster_params)

    if @monster.save
      redirect_to monsters_path, success: 'Successfully created new monster'
    else
      flash.now[:danger] = 'Unable to create new monster'
      @page_title = 'New Monster'
      render :new
    end
  end

  def edit
    @page_title = 'Edit Monster'
  end

  def update
    if @monster.update_attributes(monster_params)
      redirect_to monsters_path, success: 'Successfully updated monster'
    else
      flash.now[:danger] = 'Unable to update monster'
      @page_title = 'Edit Monster'
      render :edit
    end
  end

  def destroy
    message = if @monster.destroy
                { success: 'Successfully destroyed monster' }
              else
                { danger: 'Unable to destroy monster' }
              end

    redirect_to monsters_path, message
  end

  private

  def monster_params
    params.require(:monster).permit(:name, :power, :element, :team_id)
  end

  def find_monster
    @monster = current_user.monsters.find(params[:id])
  end
end
