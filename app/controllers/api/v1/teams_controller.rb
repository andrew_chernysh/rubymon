class Api::V1::TeamsController < Api::V1::ApiController
  respond_to :json

  def index
    respond_with current_api_user.teams
  end

  def show
    respond_with current_api_user.teams.find(params[:id])
  end

  def create
    @team = current_api_user.teams.build(team_params)

    if @team.save
      render json: @team, status: 201, location: [:api, @team]
    else
      render json: { errors: @team.errors }, status: 422
    end
  end

  def update
    @team = current_api_user.teams.find(params[:id])

    if @team.update_attributes(team_params)
      render json: @team, status: 200, location: [:api, @team]
    else
      render json: { errors: @team.errors }, status: 422
    end
  end

  def destroy
    @team = current_api_user.teams.find(params[:id])
    @team.destroy
    head 204
  end

  private

  def team_params
    params.require(:team).permit(:title, monster_ids: [])
  end
end
