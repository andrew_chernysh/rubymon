class Api::V1::ApiController < ApplicationController
  before_filter :authorize_with_token!

  rescue_from StandardError do
    render json: { errors: 'Bad request' }, status: :bad_request
  end

  private

  def current_api_user
    @current_api_user ||= begin
      auth_header_token = request.headers['Authorization']
      auth_header_token.present? ? User.find_by_token(auth_header_token) : nil
    end
  end

  def authorize_with_token!
    if current_api_user.blank?
      render json: { errors: 'Not authenticated' }, status: :unauthorized
    end
  end
end
