class Api::V1::MonstersController < Api::V1::ApiController
  respond_to :json

  def index
    respond_with current_api_user.monsters
  end

  def show
    respond_with current_api_user.monsters.find(params[:id])
  end

  def create
    @monster = current_api_user.monsters.build(monster_params)

    if @monster.save
      render json: @monster, status: 201, location: [:api, @monster]
    else
      render json: { errors: @monster.errors }, status: 422
    end
  end

  def update
    @monster = current_api_user.monsters.find(params[:id])

    if @monster.update_attributes(monster_params)
      render json: @monster, status: 200, location: [:api, @monster]
    else
      render json: { errors: @monster.errors }, status: 422
    end
  end

  def destroy
    @monster = current_api_user.monsters.find(params[:id])
    @monster.destroy!
    head 204
  end

  private

  def monster_params
    params.require(:monster).permit(:name, :power, :element, :team_id)
  end
end
