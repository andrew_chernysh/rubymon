class BaseController < ApplicationController
  add_flash_types :success, :danger

  before_filter :authenticate_user!

  private

  helper_method :current_user, :signed_in?

  def current_user
    @current_user ||= begin
      session_user_id = session[:user_id]
      session_user_id.present? ? User.find_by_id(session_user_id) : nil
    end
  end

  def signed_in?
    current_user.present?
  end

  def authenticate_user!
    redirect_to sign_in_path unless signed_in?
  end
end
