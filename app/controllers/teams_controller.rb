class TeamsController < BaseController
  before_filter :find_team, only: [:show, :edit, :update, :destroy]

  def index
    @teams = Effective::Datatables::Teams.new(user_id: current_user.id)
    @page_title = 'Teams'
  end

  def show
    @team_monsters = Effective::Datatables::Monsters.new(user_id: current_user.id, team_id: @team.id)
    @page_title = @team.title
  end

  def new
    @team = current_user.teams.build
    @page_title = 'New Team'
  end

  def create
    @team = current_user.teams.build(team_params)

    if @team.save
      redirect_to teams_path, success: 'Successfully created new team'
    else
      flash.now[:danger] = 'Unable to create new team'
      @page_title = 'New Team'
      render :new
    end
  end

  def edit
    @page_title = 'Edit Team'
  end

  def update
    if @team.update_attributes(team_params)
      redirect_to teams_path, success: 'Successfully updated team'
    else
      flash.now[:danger] = 'Unable to update new team'
      @page_title = 'Edit Team'
      render :edit
    end
  end

  def destroy
    message = if @team.destroy
                { success: 'Successfully destroyed team' }
              else
                { danger: 'Unable to destroy team' }
              end

    redirect_to teams_path, message
  end

  private

  def team_params
    params.require(:team).permit(:title, monster_ids: [])
  end

  def find_team
    @team = current_user.teams.find(params[:id])
  end
end
