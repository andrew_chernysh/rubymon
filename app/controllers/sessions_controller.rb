class SessionsController < BaseController
  skip_before_filter :authenticate_user!, only: [:new, :auth_callback]

  def new
    @page_title = 'Sign In'
  end

  def auth_callback
    @user = User.from_omniauth!(env['omniauth.auth'])
    session[:user_id] = @user.id

    redirect_to root_path, success: 'Successfully authenticated user'
  rescue => e
    redirect_to sign_in_path, danger: "Unable to authenticate user. Error happened: #{e.message}"
  end

  def destroy
    session[:user_id] = nil

    redirect_to sign_in_path, success: 'Successfully signed out user'
  end
end
