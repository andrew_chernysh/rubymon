Rails.application.routes.draw do
  get :sign_in, to: 'sessions#new'
  get 'auth/facebook/callback', to: 'sessions#auth_callback'
  delete :sign_out, to: 'sessions#destroy'

  namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/' do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :monsters, only: [:index, :show, :create, :update, :destroy]
      resources :teams, only: [:index, :show, :create, :update, :destroy]
    end
  end

  resources :monsters, except: [:show]
  resources :teams

  root 'monsters#index'
end
