# We are initialising the class with an options hash, which will contain the version of the api,
# and a default value for handling the default version. We provide a matches? method which
# the router will trigger for the constraint to see if the default version is required
# or the Accept header matches the given string.
#
# Example:
#
#   require 'api_constraints'
#
#   Rubymon::Application.routes.draw do
#     namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/'  do
#       scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
#         # We are going to list our resources here
#       end
#     end
#   end
#
#
# The configuration above handles versioning through headers, and the version is set to 1 and
# is the default one, so every request will be redirected to that version, no matter if the header
# with the version is present or not.

class ApiConstraints
  def initialize(version:, default: false)
    @version = version
    @default = default
  end

  def matches?(req)
    @default || req.headers['Accept'].include?("application/rubymon.v#{@version}")
  end
end
